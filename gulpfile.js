var gulp = require('gulp');
var bower = require('gulp-bower');
var elixir = require('laravel-elixir');
var livereload = require('laravel-elixir-livereload');

gulp.task('bower', function() {
    return bower();
});

var paths = {
    'jquery': 'vendor/jquery/dist',
    'bootstrap': 'vendor/bootstrap/dist',
    'bootswatch': 'vendor/bootswatch/lumen',
    'fontawesome': 'vendor/font-awesome',
    'colorbox': 'vendor/jquery-colorbox',
    'dataTables': 'vendor/datatables/media',
    'dataTablesBootstrap3Plugin': 'vendor/datatables-bootstrap3-plugin/media',
    'datatablesResponsive': 'vendor/datatables-responsive',
    'jquery_ui':  'vendor/jquery-ui',
    'datetimepicker': 'vendor/datetimepicker'
};
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {

    // Run bower install
    mix.task('bower');

    // Copy fonts straight to public
    //mix.copy('resources/' + paths.bootstrap + '/fonts/bootstrap/**', 'public/fonts');
    //mix.copy('resources/' + paths.fontawesome + '/fonts/**', 'public/fonts');

    // Copy images straight to public
    mix.copy('resources/' + paths.colorbox + '/example3/images/**', 'public/css/images');
    mix.copy('resources/' + paths.jquery_ui + '/themes/base/images/**', 'public/css/images');


    // Merge Site CSSs.
    mix.styles([
        '../../' + paths.colorbox + '/example3/colorbox.css',
        '../../' + paths.dataTables + '/css/dataTables.bootstrap.css',
        '../../' + paths.dataTablesBootstrap3Plugin + '/css/datatables-bootstrap3.css',
        '../../' + paths.datetimepicker + '/jquery.datetimepicker.css'
    ], 'public/css/styles.css');

    // Merge Site scripts.
    mix.scripts([
        '../../' + paths.colorbox + '/jquery.colorbox.js',
        '../../' + paths.dataTables + '/js/jquery.dataTables.js',
        '../../' + paths.dataTables + '/js/dataTables.bootstrap.js',
        '../../' + paths.dataTablesBootstrap3Plugin + '/js/datatables-bootstrap3.js',
        '../../' + paths.datatablesResponsive + '/js/dataTables.responsive.js',
        '../../' + paths.datetimepicker + '/jquery.datetimepicker.js',
        'bootstrap-dataTables-paging.js',
        'dataTables.bootstrap.js',
        'datatables.fnReloadAjax.js'
    ], 'public/js/site.js');

    mix.livereload(['public/css/styles.css']);
});
