<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        Model::unguard();
        $this->call('UserTableSeeder');

        $this->command->info('User table seeded!');
        Model::reguard();
    }
}

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'email' => 'hanief@gmail.com',
            'name'  => 'Hanief',
            'password' => Hash::make('R4h4s14sumed4ng'),
            'approved' => true,
            'admin' => true
        ]);

        User::create([
            'email' => 'agungbaskoro86@gmail.com',
            'name'  => 'Agung Baskoro',
            'password' => Hash::make('nusantara'),
            'approved' => true,
            'admin' => false
        ]);

        User::create([
            'email' => 'rosidawatipriana@gmail.com',
            'name'  => 'Rosidawati',
            'password' => '$2y$10$OnX.5emBLrgs835PuP9TMO63UybB815cMjodWHYRxTHz9zeLY2XUi',
            'approved' => true,
            'admin' => false
        ]);

        User::create([
            'email' => 'hendartvd@gmail.com',
            'name'  => 'Vebra DH',
            'password' => '$2y$10$AkTrl1duskPSwR6Y6QujleS6NdUFjCJk.d3O0U6KPP7rJ6dYawNRK',
            'approved' => true,
            'admin' => false
        ]);
    }

}
