<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('to');
            $table->string('msisdn');
            $table->string('messageId');
            $table->string('message-timestamp');
            $table->string('text');
            $table->string('keyword');
            $table->string('concat');
            $table->string('concat-ref');
            $table->string('concat-total');
            $table->string('concat-part');
            $table->string('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('messages');
    }
}
