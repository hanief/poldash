<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhonebooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phonebooks', function (Blueprint $table) {
            $table->integer('id')->unique();
            $table->string('nama');
            $table->string('alamat');
            $table->string('kota');
            $table->string('hp');
            $table->string('agama');
            $table->string('gender');
            $table->string('lahir');
            $table->string('pekerjaan');
            $table->string('grup');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('phonebooks');
    }
}
