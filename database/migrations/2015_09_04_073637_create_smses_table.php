<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smses', function(Blueprint $table)
        {
            $table->integer('id')->unique();
            $table->string('tanggal');
            $table->string('waktu');
            $table->string('pesan');
            $table->string('pengirim');
            $table->timestamps();

            $table->index('pesan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('smses');
    }
}
