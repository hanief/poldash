<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phonebook extends Model
{
    protected $fillable = [
        'id',
        'nama',
        'alamat',
        'kota',
        'hp',
        'agama',
        'gender',
        'lahir',
        'pekerjaan',
        'grup'
    ];
}
