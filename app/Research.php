<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Research extends Model
{
    protected $fillable = [
        'title',
        'type',
        'area',
        'start_date',
        'end_date',
        'user_id'
    ];

    public function rules()
    {
        return $this->hasMany('App\Rule');
    }

    public function datapoints()
    {
        return $this->hasMany('App\Datapoint');
    }

    public function persons()
    {
        return $this->hasMany('App\Person');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
