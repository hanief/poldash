<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');

Route::get('/dashboard', [
    'middleware' => 'auth',
    'uses' => 'DashboardController@index'
]);

Route::get('/callback', [
    'middleware' => 'auth',
    'uses' => 'DashboardController@callback'
]);

Route::get('/dashboard/ddc', [
    'middleware' => 'auth',
    'uses' => 'DashboardController@ddc'
]);

Route::get('/dashboard/survey', [
    'middleware' => 'auth',
    'uses' => 'DashboardController@survey'
]);

Route::get('/dashboard/survey/{daerah}', [
    'middleware' => 'auth',
    'uses' => 'DashboardController@surveyByDaerah'
]);

Route::get('/dashboard/ddc/test', [
    'middleware' => 'auth',
    'uses' => 'DashboardController@ddctest'
]);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

Route::resource('sms', 'SmsController');

Route::get('api/sms/all', 'ApiSmsController@index');
Route::get('api/sms/read', 'ApiSmsController@read');
Route::get('api/sms/unread', 'ApiSmsController@unread');
Route::get('api/sms/{area}/daily', 'ApiSmsController@dailySms');
Route::post('api/sms/new', 'ApiSmsController@newSms');
Route::get('api/sms/new', 'ApiSmsController@newSms');

Route::get('api/ddc/aggregates', 'ApiSmsController@aggregates');
Route::get('api/ddc/aggregates/{questionId}','ApiSmsController@aggByQuestionId');
Route::get('api/ddc/aggregates/sender{format?}', 'ApiSmsController@aggSender');
Route::get('api/ddc/aggregates/sender/daily', 'ApiSmsController@aggSenderDaily');
Route::get('api/ddc/aggregates/sender/daily/graph', 'ApiSmsController@aggSenderDailyGraph');
Route::get('api/ddc/aggregates/sender/{date}', 'ApiSmsController@aggSenderWithDate');
Route::get('api/ddc/aggregates/bydate', 'ApiSmsController@aggByDate');

Route::get('api/survey/{daerah}/aggregates', 'ApiSmsController@surveyAggByDaerah');

Route::get('api/survey/{daerah}/dashboard/agg/{nomor}', 'ApiSmsController@surveyAggForDashboard');
Route::get('api/survey/{daerah}/dashboard/sms', 'ApiSmsController@surveySmsForDashboard');
Route::get('api/survey/{daerah}/dashboard/sender', 'ApiSmsController@surveySenderForDashboard');
Route::get('api/survey/{daerah}/dashboard/percentage', 'ApiSmsController@surveyPercentageForDashboard');
Route::get('api/survey/{daerah}/dashboard/gauge', 'ApiSmsController@surveyForDashboardGauge');
Route::get('api/survey/{daerah}/dashboard/{nomor}', 'ApiSmsController@surveyForDashboard');

Route::get('api/research/{id}/dashboard/gauge', 'ApiSmsController@researchForDashboardGauge');
Route::get('api/research/{id}/dashboard/{nomor}', 'ApiSmsController@researchForDashboard');
Route::get('api/research/{area}/agg/sender/daily', 'ApiSmsController@aggSenderDailyPerArea');

Route::resource('research', 'ResearchController');

Route::resource('research/{researchId}/rules', 'RuleController', ['names' => [
    'store' => 'rules.store',
    'create' => 'rules.create',
    'index' => 'rules.index',
    'update' => 'rules.update'
]]);

Route::resource('research/{researchId}/rules/{ruleId}/questions', 'QuestionController', ['names' => [
    'store' => 'questions.store',
    'create' => 'questions.create',
    'index' => 'questions.index',
    'update' => 'questions.update',
    'edit' => 'questions.edit',
    'destroy' => 'questions.destroy'
]]);

Route::resource('research/{researchId}/rules/{ruleId}/questions/{questionId}/options', 'OptionController', ['names' => [
    'store' => 'options.store',
    'create' => 'options.create',
    'index' => 'options.index',
    'update' => 'options.update',
    'edit' => 'options.edit',
    'destroy' => 'options.destroy'
]]);

Route::get('reports/reports', ['as' => 'reports.reports', 'uses' => 'ReportController@reports']);
Route::get('reports/{id}/approve', ['as' => 'reports.approve', 'uses' => 'ReportController@approve']);
Route::resource('reports', 'ReportController');

Route::get('quick-count', ['as' => ' quickcount.index', 'uses' => 'ResearchController@quickCount']);

Route::get('quick-count/qcs/{daerah}', ['as' => ' quickcount.qcs', 'uses' => 'ResearchController@qcs']);

Route::get('quick-count/{daerah}', ['as' => 'quickcount.daerah', 'uses' => 'ResearchController@qcByArea']);