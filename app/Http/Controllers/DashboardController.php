<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return view('dashboard.index');
    }

    public function callback()
    {
        return view('dashboard.callback');
    }

    public function ddc()
    {
        return view('dashboard.ddc');
    }

    public function ddctest()
    {
        return view('dashboard.test');
    }

    public function survey()
    {
        return redirect('/dashboard/survey/kutim');
    }

    public function surveyByDaerah($daerah)
    {
        $questions = [];

        if ($daerah == 'kutim') {
            $questions = [
                '1. Apabila Pilkada Kutai Timur dilaksanakah saat ini, pasangan manakah yang Bapak/Ibu/Saudara pilih?',
                '2. Jika kandidat dalam pilkada 2 pasangan sebagaimana terlihat di bawah ini, di antara 2 pasangan kandidat di bawah ini siapa yang akan Bapak/ Ibu/ Saudara pilih? (Responden ditunjukkan alat peraga)'
            ];
        } else if ($daerah == 'nasional') {
            $questions = [
                '1. Bagaimana penilaian Bapak/Ibu/Saudara terhadap kinerja pemerintahan Joko Widodo-Jusuf Kalla?',
                '2. Bagaimana penilaian Bapak/Ibu/Saudara terhadap 1 tahun kinerja Presiden Joko Widodo (Jokowi)?'
            ];
        }


        return view('dashboard.survey')->with(['daerah' => $daerah, 'questions' => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
