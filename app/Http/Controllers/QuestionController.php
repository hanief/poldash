<?php

namespace App\Http\Controllers;

use App\Question;
use App\Rule;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class QuestionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($researchId, $ruleId)
    {
        $rule = Rule::findOrFail($ruleId);

        $questions = $rule->questions;

        return view('question.index')->with(['researchId' => $researchId, 'rule' => $rule, 'questions' => $questions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($researchId, $ruleId)
    {
        return view('question.create')->with(['researchId' => $researchId, 'ruleId' => $ruleId]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($researchId, $ruleId, Request $request)
    {
        $rule = Rule::find($ruleId);

        $question = $rule->questions()->create($request->all());;

        return redirect('research/'.$researchId.'/rules/'.$ruleId.'/questions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($researchId, $ruleId, $id)
    {
        $question = Question::find($id);

        return view('question.edit')->with(['researchId'=> $researchId, 'ruleId' => $ruleId, 'question' => $question]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $researchId, $ruleId, $id)
    {
        $question = Question::findOrFail($id);

        $question->update($request->all());

        return redirect('research/'.$researchId.'/rules/'.$ruleId.'/questions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($researchId, $ruleId, $id)
    {
        $question = Question::findOrFail($id);

        $question->delete();

        return redirect('research/'.$researchId.'/rules/'.$ruleId.'/questions');
    }
}
