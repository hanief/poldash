<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Research;
use App\Rule;

class RuleController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($researchId)
    {
        $research = Research::findOrFail($researchId);

        $rules = $research->rules;

        return view('rule.index')->with(['research' => $research, 'rules' => $rules]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $researchId)
    {
        $research = Research::find($researchId);

        $rule = $research->rules()->create($request->all());;

        return redirect('research/'.$researchId.'/rules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($researchId,$id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($researchId,$id)
    {
        $research = Research::findOrFail($researchId);

        $rule = Rule::findOrFail($id);

        return view('rule.edit')->with(['research' => $research, 'rule' => $rule]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $researchId, $id)
    {
        $rule = Rule::findOrFail($id);

        $rule->update($request->all());

        return redirect('research/'.$researchId.'/rules');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($researchId, $id)
    {
        $rule = Rule::findOrFail($id);
        $rule->delete();

        return redirect('research/'.$researchId.'/rules');
    }
}
