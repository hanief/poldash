<?php

namespace App\Http\Controllers;

use App\Report;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Research;
use App\Person;
use Auth;
use yajra\Datatables\Datatables;

class ResearchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $researches = Research::all();

        return view('research.index')->with('researches', $researches);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('research.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $research = Auth::user()->researches()->create($request->all());;

        return redirect('research');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        return view('research.show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $research = Research::findOrFail($id);

        return view('research.edit')->with('research', $research);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $research = Research::findOrFail($id);

        $research->update($request->all());

        return redirect('research');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $research = Research::findOrFail($id);
        $research->delete();

        return redirect('research');
    }

    public function quickCount()
    {
        return view('qc.index');
    }

    public function qcs($daerah)
    {
        $kota = [
            'bateng' => 'Bangka Tengah',
            'babar' => 'Bangka Barat',
            'basel' => 'Bangka Selatan',
            'beltim' => 'Belitung Timur',
            'kutim' => 'Kutai Timur'
        ];

        $persons = Person::where(['persons.kota' => $kota[$daerah]])
            ->leftJoin('reports', 'reports.pengirim', '=', 'persons.hp')
            ->select('persons.id', 'persons.hp', 'persons.nama', 'persons.alamat', 'reports.id as report_id', 'reports.pesan', 'reports.waktu', 'reports.status');

        return Datatables::of($persons)
            ->addColumn('aksi', function ($person) {
                if ($person->status === 1) {
                    return '<a class="btn btn-sm btn-success" href=' . route('reports.approve', $person->report_id) . '><span class="glyphicon glyphicon-ok"></span></a>
                        <a class="btn btn-sm btn-info" href=' . route('reports.edit', $person->report_id) . '><span class="glyphicon glyphicon-pencil"></span></a>';
                } else {
                    return '<a class="btn btn-sm btn-primary" href=' . route('reports.create') . '><span class="glyphicon glyphicon-plus"></span></a>';
                }
            })
            ->addColumn('count', function($person) {
                if (count_chars($person->pesan) > 1) {
                    return count(explode('#', $person->pesan));
                } else {
                    return 0;
                }
            })
            ->addColumn('status', function($person) {
                if (is_null($person->status)) {
                    return '';
                } else {
                    $status = array('Pending', 'Approved');
                    return $status[$person->status];
                }
            })
            ->make(true);
    }

    public function qcByArea($daerah) {
        $area = [
            'bateng' => 'Bangka Tengah',
            'basel' => 'Bangka Selatan',
            'babar' => 'Bangka Barat',
            'beltim' => 'Belitung Timur',
            'kutim' => 'Kutai Timur'
        ];

        return view ('qc.daerah')->with(['daerah' => $daerah, 'area' => $area[$daerah]]);
    }
}
