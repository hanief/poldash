<?php

namespace App\Http\Controllers;

use App\Datapoint;
use App\Report;
use App\Research;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('report.index');
    }

    public function reports()
    {
        $dateArray = array();
        for($i = 0; $i < 7; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }

        $reports = Report::whereRaw('(tanggal = "'.$dateArray[0]
            .'" OR tanggal = "'.$dateArray[1]
            .'" OR tanggal = "'.$dateArray[2]
            .'" OR tanggal = "'.$dateArray[3]
            .'" OR tanggal = "'.$dateArray[4]
            .'" OR tanggal = "'.$dateArray[5]
            .'" OR tanggal = "'.$dateArray[6].'"
            ) AND status = FALSE')
            ->get();

        return Datatables::of($reports)
            ->addColumn('aksi', function ($report) {
                if ($report->status) {
                    return '<a class="btn btn-sm btn-info" href='.route('reports.edit', $report->id).'><span class="glyphicon glyphicon-pencil"></span></a>';
                } else {
                    return '<a class="btn btn-sm btn-success" href='.route('reports.approve', $report->id).'><span class="glyphicon glyphicon-ok"></span></a>
                        <a class="btn btn-sm btn-info" href='.route('reports.edit', $report->id).'><span class="glyphicon glyphicon-pencil"></span></a>
                        <a class="btn btn-sm btn-danger" href='.route('reports.destroy', $report->id).' data-method="delete" data-token='.csrf_token().' data-confirm="Are you sure?"><span class="glyphicon glyphicon-trash"></span></a>';
                }
            })
            ->addColumn('count', function($report) {
                return count(explode('#', $report->pesan));
            })
            ->editColumn('status', '{{$status ? "Approved" : "Pending"}}')
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('report.create')->with('report', new Report());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $report = Report::find($id);

        return view('report.edit')->with('report', $report);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report = Report::find($id);

        $report->tanggal = $request->input('tanggal');
        $report->waktu = $request->input('waktu');
        $report->pesan = $request->input('pesan');
        $report->pengirim = $request->input('pengirim');
        $report->status = $request->input('status');

        $report->save();

        return redirect('reports');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::findOrFail($id);

        $report->delete();

        return redirect('reports');
    }

    public function approve($id)
    {
        $report = Report::findOrFail($id);
        $report->status = true;
        $report->save();

        $this->parseReport($report);

        return redirect('reports');
    }

    public function parseReport(Report $report)
    {
        $researches = Research::with('rules.questions.options')->get();

        $data = explode('#', $report->pesan);

        foreach ($researches as $research) {
            foreach ($research->rules as $rule) {
                if (strtolower($rule->keyword) === strtolower($data[0])) {
                    $datapoint = Datapoint::where('report_id',$report->id)->first();
                    if(is_null($datapoint)) {
                        $datapoint = $report->datapoints()->create([
                            'kabupaten' => $research->area,
                            'daerah' => $research->area,
                            'tanggal' => $report->tanggal,
                            'research_id' => $research->id
                        ]);

                        foreach ($rule->questions as $question) {
                            if (count($data) >= (int)$question->number + 1) {
                                $answer = $datapoint->answers()->create([
                                    'kuesioner_id' => (int)$data[1],
                                    'pertanyaan_id' => (int)$question->number,
                                    'jawaban' => $data[(int)$question->number + 1],
                                    'deskripsi' => ''
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }
}
