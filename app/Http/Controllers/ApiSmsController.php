<?php

namespace App\Http\Controllers;


use App\Message;
use App\Research;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Nathanmac\Utilities\Parser\Parser;
use Illuminate\Support\Facades\DB;
use App\Sms;
use App\Answer;
use App\Survey;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Log;

class ApiSmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        for($i = 0; $i < 7; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }

        $smses = DB::table('smses')
            ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
            ->select('smses.id', 'smses.tanggal', 'smses.waktu', 'phonebooks.nama', 'smses.pengirim as hp', 'smses.pesan')
            ->orderBy('smses.tanggal', 'desc')
            ->whereRaw('smses.tanggal = "'.$dateArray[0].
                '" OR smses.tanggal = "'.$dateArray[1].
                '" OR smses.tanggal = "'.$dateArray[2].
                '" OR smses.tanggal = "'.$dateArray[3].
                '" OR smses.tanggal = "'.$dateArray[4].
                '" OR smses.tanggal = "'.$dateArray[5].
                '" OR smses.tanggal = "'.$dateArray[6].'"')
            ->get();

        return $smses;
    }

    public function dailySms($area)
    {
        $agg = array();
        $dateArray = array();
        for($i = 0; $i < 7; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }

        if ($area === 'all') {
            $agg = DB::table('smses')
                ->selectRaw('
             COUNT(IF(smses.tanggal = "' . $dateArray[6] . '", smses.tanggal, NULL)) AS "' . $dateArray[6] . '",
             COUNT(IF(smses.tanggal = "' . $dateArray[5] . '", smses.tanggal, NULL)) AS "' . $dateArray[5] . '",
             COUNT(IF(smses.tanggal = "' . $dateArray[4] . '", smses.tanggal, NULL)) AS "' . $dateArray[4] . '",
             COUNT(IF(smses.tanggal = "' . $dateArray[3] . '", smses.tanggal, NULL)) AS "' . $dateArray[3] . '",
             COUNT(IF(smses.tanggal = "' . $dateArray[2] . '", smses.tanggal, NULL)) AS "' . $dateArray[2] . '",
             COUNT(IF(smses.tanggal = "' . $dateArray[1] . '", smses.tanggal, NULL)) AS "' . $dateArray[1] . '",
             COUNT(IF(smses.tanggal = "' . $dateArray[0] . '", smses.tanggal, NULL)) AS "' . $dateArray[0] . '"
            ')
                ->get();
        } else {
            $agg = DB::table('smses')
                ->selectRaw('
                 COUNT(IF(smses.tanggal = "' . $dateArray[6] . '", smses.tanggal, NULL)) AS "' . $dateArray[6] . '",
                 COUNT(IF(smses.tanggal = "' . $dateArray[5] . '", smses.tanggal, NULL)) AS "' . $dateArray[5] . '",
                 COUNT(IF(smses.tanggal = "' . $dateArray[4] . '", smses.tanggal, NULL)) AS "' . $dateArray[4] . '",
                 COUNT(IF(smses.tanggal = "' . $dateArray[3] . '", smses.tanggal, NULL)) AS "' . $dateArray[3] . '",
                 COUNT(IF(smses.tanggal = "' . $dateArray[2] . '", smses.tanggal, NULL)) AS "' . $dateArray[2] . '",
                 COUNT(IF(smses.tanggal = "' . $dateArray[1] . '", smses.tanggal, NULL)) AS "' . $dateArray[1] . '",
                 COUNT(IF(smses.tanggal = "' . $dateArray[0] . '", smses.tanggal, NULL)) AS "' . $dateArray[0] . '"')
                ->whereRaw('smses.pesan LIKE "%'.$area.'%"')
                ->get();
        }

        return $agg;
    }

    public function newSms(Request $request)
    {
        //Log::info($request);
        //Log::info($request->all());
        $message = Message::create([
            'type' => $request['type'],
            'to' => $request['to'],
            'msisdn' => $request['msisdn'],
            'messageId' => $request['messageId'],
            'message-timestamp' => $request['message-timestamp'],
            'text' => $request['text'],
            'keyword' => $request['keyword']
        ]);

        if ($request->has('concat')) {
            $message['concat'] = $request['concat'];
            $message['concat-ref'] = $request['concat-ref'];
            $message['concat-total'] = $request['concat-total'];
            $message['concat-part'] = $request['concat-part'];
            $message->save();
        }

        return response()->json(['code' => 200, 'message' => $message]);
    }

    public function read()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://poltracking.zenziva.com/api/inboxgetall.php?userkey=90gu40t&passkey=fa83ts&status=read');

        $parser = new Parser();
        $parsed = $parser->xml($response->getBody());

        return $parsed;
    }

    public function unread()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://poltracking.zenziva.com/api/readsms.php?userkey=90gu40t&passkey=fa83ts');

        $parser = new Parser();
        $parsed = $parser->xml($response->getBody());

        return $parsed;
    }

    public function aggregates()
    {
        $aggregates = array();
        for ($i = 1; $i < 7; $i++) {
            $answers = array();

            $answers = DB::table('answers')
                ->selectRaw('jawaban, count(jawaban) as jumlah')
                ->where('pertanyaan_id', $i)
                ->groupBy('jawaban')
                ->orderBy('jumlah', 'desc')
                ->get();
//                ->lists('jumlah', 'jawaban');

            //$aggregates[] = array('jawaban' => array_keys($answers), 'jumlah' => array_values($answers));
            $aggregates[] = $answers;
        }

        return $aggregates;
    }

    public function aggByQuestionId($questionId)
    {
        $answers = DB::table('answers')
                ->selectRaw('jawaban, count(jawaban) as jumlah')
                ->where('pertanyaan_id', $questionId)
                ->groupBy('jawaban')
                ->having('jumlah', '>', 50)
                ->orderBy('jumlah', 'desc')
                ->get();

//        return $answers;

        $datapts = array();

        foreach($answers as $element) {
            $datapt = array("title" => $element->jawaban, "value" => $element->jumlah);
            array_push($datapts, $datapt);
        }

        $dataseq = [['title' =>  'KT', 'datapoints' => $datapts]];

        $title = "";

        if ($questionId == 1) {
            $title = "Respon tuan rumah";
        } else if ($questionId == 2) {
            $title= "Pilihan calon bupati";
        } else if ($questionId == 3) {
            $title = "Popularitas Norbaiti";
        } else if ($questionId == 4) {
            $title = "Elektabilitas Norbaiti";
        } else if ($questionId == 5) {
            $title = "Aspirasi";
        } else if ($questionId == 6) {
            $title = "Stiker ditempel";
        }

        $json = ["graph" => ["title" => $title, "type" => "bar", "datasequences" => $dataseq]];

        return $json;
    }

    public function aggSender($format)
    {
        $agg = array();

        $agg = DB::table('smses')
            ->selectRaw('smses.pengirim as nomor, phonebooks.nama, count(smses.pengirim) as jumlah')
            ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
            ->groupBy('smses.pengirim')
            ->orderBy('jumlah', 'desc')
            ->get();

        if ($format === '.csv') {
            return Excel::create('sender', function($excel) use($agg) {

                $excel->sheet('sender', function($sheet) use($agg) {

                    $sheet->fromArray($agg);
                });

            })->export('csv');
        } else {
            return $agg;
        }
    }

    public function aggSenderWithDate($date)
    {
        $agg = array();
        $today = date('Y-m-d');

        if ($date === 'day') {
            $agg = DB::table('smses')
                ->selectRaw('smses.pengirim as nomor, phonebooks.nama, count(smses.pesan) as jumlah')
                ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
                ->whereRaw('STR_TO_DATE(smses.tanggal, "%Y-%m-%d") = CURDATE()')
                ->groupBy('smses.pengirim')
                ->orderBy('jumlah', 'desc')
                ->get();
        } else if ($date === 'week') {
            $agg = DB::table('smses')
                ->selectRaw('smses.pengirim as nomor, phonebooks.nama, count(smses.pengirim) as jumlah')
                ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
                ->whereRaw('STR_TO_DATE(smses.tanggal, "%Y-%m-%d") > CURDATE() - INTERVAL 7 DAY')
                ->groupBy('smses.pengirim')
                ->orderBy('jumlah', 'desc')
                ->get();
        } else if ($date === 'month') {
            $agg = DB::table('smses')
                ->selectRaw('smses.pengirim as nomor, phonebooks.nama, count(smses.pengirim) as jumlah')
                ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
                ->whereRaw('STR_TO_DATE(smses.tanggal, "%Y-%m-%d") > CURDATE() - INTERVAL 1 MONTH')
                ->groupBy('smses.pengirim')
                ->orderBy('jumlah', 'desc')
                ->get();
        } else if ($date === 'year') {
            $agg = DB::table('smses')
                ->selectRaw('smses.pengirim as nomor, phonebooks.nama, count(smses.pengirim) as jumlah')
                ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
                ->whereRaw('STR_TO_DATE(smses.tanggal, "%Y-%m-%d") > CURDATE() - INTERVAL 1 YEAR')
                ->groupBy('smses.pengirim')
                ->orderBy('jumlah', 'desc')
                ->get();
        }

        return $agg;
    }

    public function aggByDate()
    {
        $agg = DB::table('smses')
            ->selectRaw('smses.tanggal, count(smses.pesan) as jumlah')
            ->groupBy('smses.tanggal')
            ->get();

        return $agg;
    }

    public function aggSenderDaily()
    {
        $agg = array();
        $dateArray = array();

        for($i = 0; $i < 7; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }

        $agg = DB::table('smses')
            ->selectRaw('smses.pengirim as nomor, phonebooks.nama,
             COUNT(IF(smses.tanggal = "'.$dateArray[6].'", smses.tanggal, NULL)) AS "'.$dateArray[6].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[5].'", smses.tanggal, NULL)) AS "'.$dateArray[5].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[4].'", smses.tanggal, NULL)) AS "'.$dateArray[4].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[3].'", smses.tanggal, NULL)) AS "'.$dateArray[3].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[2].'", smses.tanggal, NULL)) AS "'.$dateArray[2].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[1].'", smses.tanggal, NULL)) AS "'.$dateArray[1].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[0].'", smses.tanggal, NULL)) AS "'.$dateArray[0].'"
            ')
            ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
            ->whereRaw('smses.tanggal = "'.$dateArray[0].
                '" OR smses.tanggal = "'.$dateArray[1].
                '" OR smses.tanggal = "'.$dateArray[2].
                '" OR smses.tanggal = "'.$dateArray[3].
                '" OR smses.tanggal = "'.$dateArray[4].
                '" OR smses.tanggal = "'.$dateArray[5].
                '" OR smses.tanggal = "'.$dateArray[6].'"')
            ->groupBy('smses.pengirim')
            ->get();

        return $agg;
    }

    public function aggSenderDailyPerArea($area)
    {
        $agg = array();
        $dateArray = array();

        for($i = 0; $i < 7; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }

        $areaOption = '';

        if ($area !== 'all')
        {
            $areaOption = 'AND smses.pesan LIKE "%'.$area.'%"';
        }

        $agg = DB::table('smses')
            ->selectRaw('smses.pengirim as nomor, phonebooks.nama, phonebooks.alamat,
             COUNT(IF(smses.tanggal = "'.$dateArray[6].'", smses.tanggal, NULL)) AS "'.$dateArray[6].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[5].'", smses.tanggal, NULL)) AS "'.$dateArray[5].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[4].'", smses.tanggal, NULL)) AS "'.$dateArray[4].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[3].'", smses.tanggal, NULL)) AS "'.$dateArray[3].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[2].'", smses.tanggal, NULL)) AS "'.$dateArray[2].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[1].'", smses.tanggal, NULL)) AS "'.$dateArray[1].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[0].'", smses.tanggal, NULL)) AS "'.$dateArray[0].'",
             COUNT(IF((smses.tanggal = "'.$dateArray[0].'") OR (smses.tanggal = "'.$dateArray[1].'") OR
             (smses.tanggal = "'.$dateArray[2].'") OR (smses.tanggal = "'.$dateArray[3].'") OR
             (smses.tanggal = "'.$dateArray[4].'") OR (smses.tanggal = "'.$dateArray[5].'") OR
             (smses.tanggal = "'.$dateArray[6].'"), smses.tanggal, NULL)) AS "total"')
            ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
            ->whereRaw('(smses.tanggal = "'.$dateArray[0].
                '" OR smses.tanggal = "'.$dateArray[1].
                '" OR smses.tanggal = "'.$dateArray[2].
                '" OR smses.tanggal = "'.$dateArray[3].
                '" OR smses.tanggal = "'.$dateArray[4].
                '" OR smses.tanggal = "'.$dateArray[5].
                '" OR smses.tanggal = "'.$dateArray[6].
                '") '.$areaOption)
            ->groupBy('smses.pengirim')
            ->get();

        return $agg;
    }

    public function aggSenderDailyGraph()
    {
        $agg = array();
        $dateArray = array();
        for($i = 0; $i < 7; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }


        $agg = DB::table('smses')
            ->selectRaw('smses.pengirim as nomor, phonebooks.nama,
             COUNT(IF(smses.tanggal = "'.$dateArray[6].'", smses.tanggal, NULL)) AS "'.$dateArray[6].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[5].'", smses.tanggal, NULL)) AS "'.$dateArray[5].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[4].'", smses.tanggal, NULL)) AS "'.$dateArray[4].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[3].'", smses.tanggal, NULL)) AS "'.$dateArray[3].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[2].'", smses.tanggal, NULL)) AS "'.$dateArray[2].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[1].'", smses.tanggal, NULL)) AS "'.$dateArray[1].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[0].'", smses.tanggal, NULL)) AS "'.$dateArray[0].'"
            ')
            ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
            ->whereRaw('smses.pesan LIKE "SRV#ADD%"')
            ->groupBy('smses.pengirim')
            ->get();

        $dataseq = array();

        foreach($agg as $element) {
            $datapts = array();

            for($i = 0; $i < 7; $i++) {

                $datapt = array("title" => $dateArray[$i], "value" => $element->$dateArray[$i]);

                array_push($datapts, $datapt);
            }

            if ($element->nama) {
                $obj = ['title' =>  $element->nama, 'datapoints' => $datapts];
            } else {
                $obj = ['title' =>  $element->nomor, 'datapoints' => $datapts];
            }

            array_push($dataseq, $obj);
        }

        $json = ["graph" => ["title" => "Daily SMS", "type" => "bar", "datasequences" => $dataseq]];

        return $json;
    }

    public function export()
    {
        $csv = \League\Csv\Writer::createFromFileObject(new \SplTempFileObject());

        $csv->insertOne(\Schema::getColumnListing('sender'));

        Person::all()->each(function($person) use($csv) {
            $csv->insertOne($person->toArray());
        });

        $csv->output('sender.csv');
    }


    public function surveyAggByDaerah($daerah)
    {
        $aggregates = array();

        for ($i = 1; $i < 3; $i++) {
            $answers = array();

            $answers = DB::table('answers')
                ->selectRaw('answers.jawaban, count(answers.jawaban) as jumlah')
                ->leftJoin('surveys', 'surveys.id', '=', 'answers.survey_id')
                //->where(['answers.pertanyaan_id' => $i, 'surveys.kabupaten' => $daerah])
                ->whereRaw('answers.pertanyaan_id = '.$i.' AND surveys.kabupaten = "'.$daerah.'" AND (surveys.tanggal = "2015-10-23" OR surveys.tanggal = "2015-10-24" OR surveys.tanggal = "2015-10-25"  )')
                ->groupBy('answers.jawaban')
                ->orderBy('jumlah', 'desc')
                ->get();
            $aggregates[] = $answers;
        }

        return $aggregates;
    }

    function createDateRangeArray($strDateFrom,$strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange=array();

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('Y-m-d',$iDateFrom));
            }
        }
        return $aryRange;
    }

    public function researchForDashboard($id,$nomor)
    {
        $research = Research::with('rules.questions.options')->where('id', $id)->get()->first();

        $dateArray = $this->createDateRangeArray($research->start_date, $research->end_date);
        $dateString = '';

        foreach($dateArray as $key => $date) {
            $dateString = $dateString.'datapoints.tanggal = "'.$date.'"';
            if ($key < count($dateArray)-1) {
                $dateString = $dateString.' OR ';
            }
        }

        $answers = array();

        $answers = DB::table('answers')
            ->selectRaw('answers.jawaban, count(answers.jawaban) as jumlah')
            ->leftJoin('datapoints', 'datapoints.id', '=', 'answers.datapoint_id')
            ->whereRaw('answers.pertanyaan_id = '.$nomor.' AND ('.$dateString.') AND datapoints.research_id = '.$id)
            ->groupBy('answers.jawaban')
            ->orderBy('jumlah', 'desc')
            ->get();

        $sum = 0.0;

        foreach ($answers as $answer) {
            $sum += $answer->jumlah;
        }

        $opsi = array();

        foreach($research->rules as $rule) {
            foreach ($rule->questions as $question) {
                if ($question->number === (int)$nomor) {
                    foreach ($question->options as $option) {
                        $opsi[$option->number] = $option->description;
                    }
                }
            }
        }

        return Excel::create($id, function ($excel) use ($answers, $nomor, $sum, $opsi) {

            $excel->sheet('aggregate', function ($sheet) use ($answers, $nomor, $sum, $opsi) {

                $sheet->appendRow(array(
                    'Jawaban', 'Elektabilitas(%)'
                ));

                foreach ($answers as $answer) {
                    $sheet->appendRow(
                        array((int)$answer->jawaban <= count($opsi) ? $opsi[(int)$answer->jawaban] : $answer->jawaban, round(($answer->jumlah / $sum) * 100, 2))
                    );
                }

                $colors = array(
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9');

                $sheet->rows(array(
                    array('Color', $colors[$nomor]),
                    array('LabelShow', 1),
                    array('YAxisMax', 70)
                ));
            });

        })->export('csv');

    }

    public function researchForDashboardGauge($id)
    {
        $gauge = DB::table('smses')
            ->selectRaw('count(smses.pesan) as sms')
            ->whereRaw('smses.pesan LIKE "%'.$daerah.'%" AND (smses.tanggal = "2015-11-04" OR smses.tanggal = "2015-11-05" OR smses.tanggal = "2015-11-06" OR smses.tanggal = "2015-11-07")')
            ->first();

        return Excel::create($daerah, function($excel) use($gauge, $daerah) {

            $excel->sheet('aggregate', function($sheet) use($gauge, $daerah) {

                $sheet->appendRow(array(
                    'Sms', 'Target'
                ));

                $sheet->appendRow(array(
                    $gauge->sms, $daerah === "kutim" ? 800: 700
                ));

                $sheet->rows(array(
                    array('Color', '#9d8cf9'),
                    array('Type', 'gauge'),
                    array('LabelShow', 1),
                ));
            });

        })->export('csv');
    }



    public function surveyForDashboard($daerah,$nomor)
    {
        $answers = array();

        $answers = DB::table('answers')
            ->selectRaw('answers.jawaban, count(answers.jawaban) as jumlah')
            ->leftJoin('surveys', 'surveys.id', '=', 'answers.survey_id')
            ->whereRaw('answers.pertanyaan_id = '.$nomor.' AND surveys.kabupaten = "'.$daerah.'" AND (surveys.tanggal = "2015-11-04" OR surveys.tanggal = "2015-11-05" OR surveys.tanggal = "2015-11-06" OR surveys.tanggal = "2015-11-07" )')
            ->groupBy('answers.jawaban')
            ->orderBy('jumlah', 'desc')
            ->get();

        $sum = 0.0;

        foreach ($answers as $answer) {
            $sum += $answer->jumlah;
        }


        return Excel::create($daerah, function ($excel) use ($answers, $nomor, $sum, $daerah, $kutim) {

            $excel->sheet('aggregate', function ($sheet) use ($answers, $nomor, $sum, $daerah, $kutim) {

                $sheet->appendRow(array(
                    'Jawaban', 'Elektabilitas(%)'
                ));

                foreach ($answers as $answer) {
                    if ($answer->jawaban < 5 and $answer->jawaban > 0) {
                        $sheet->appendRow(array($answer->jawaban, round(($answer->jumlah / $sum) * 100, 2)
                        ));
                    }
                }

                $colors = array(
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9',
                    '#52ff7f', '#ff7e0e', '#9d8cf9');

                $sheet->rows(array(
                    array('Color', $colors[$nomor]),
                    array('LabelShow', 1),
                    array('YAxisMax', 70)
                ));
            });

        })->export('csv');

    }

    public function surveyForDashboardGauge($daerah)
    {
        $gauge = DB::table('smses')
            ->selectRaw('count(smses.pesan) as sms')
            ->whereRaw('smses.pesan LIKE "%'.$daerah.'%" AND (smses.tanggal = "2015-11-04" OR smses.tanggal = "2015-11-05" OR smses.tanggal = "2015-11-06" OR smses.tanggal = "2015-11-07")')
            ->first();

        return Excel::create($daerah, function($excel) use($gauge, $daerah) {

            $excel->sheet('aggregate', function($sheet) use($gauge, $daerah) {

                $sheet->appendRow(array(
                    'Sms', 'Target'
                ));

                $sheet->appendRow(array(
                    $gauge->sms, $daerah === "kutim" ? 800: 700
                ));

                $sheet->rows(array(
                    array('Color', '#9d8cf9'),
                    array('Type', 'gauge'),
                    array('LabelShow', 1),
                ));
            });

        })->export('csv');
    }

    public function surveyAggForDashboard($daerah,$nomor)
    {
        $answers = array();

        $answers = DB::table('answers')
            ->selectRaw('answers.jawaban, count(answers.jawaban) as jumlah')
            ->leftJoin('surveys', 'surveys.id', '=', 'answers.survey_id')
            ->where(['answers.pertanyaan_id' => $nomor, 'surveys.kabupaten' => $daerah, 'surveys.tanggal' => '2015-10-22'])
            ->groupBy('answers.jawaban')
            ->orderBy('jumlah', 'desc')
            ->get();

        $sum = 0.0;

        foreach ($answers as $answer) {
            $sum += $answer->jumlah;
        }

        return Excel::create($daerah, function($excel) use($answers, $nomor, $sum) {

            $excel->sheet('aggregate', function($sheet) use($answers, $nomor, $sum) {

                $sheet->appendRow(array(
                    'Jawaban', 'Jumlah(%)'
                ));

                foreach ($answers as $answer) {
                    $sheet->appendRow(array(
                        $answer->jawaban, round(($answer->jumlah/$sum)*100,2)
                    ));
                }

                $colors = array('#52ff7f','#ff7e0e','#9d8cf9');

                $sheet->rows(array(
                    array('Color', $colors[$nomor]),
                    array('Cumulative', 1),
                    array('Type', 'bar'),
                    array('LabelShow', 1),
                ));
            });

        })->export('csv');
    }

    public function surveySenderForDashboard($daerah)
    {
        $agg = array();
        $dateArray = array();
        for($i = 0; $i < 4; $i++) {
            $dateArray[] = date('Y-m-d', strtotime('-'.$i.' days'));
        }


        $agg = DB::table('smses')
            ->selectRaw('smses.pengirim as nomor, phonebooks.nama, phonebooks.alamat,
             COUNT(IF(smses.tanggal = "'.$dateArray[3].'", smses.tanggal, NULL)) AS "'.$dateArray[3].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[2].'", smses.tanggal, NULL)) AS "'.$dateArray[2].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[1].'", smses.tanggal, NULL)) AS "'.$dateArray[1].'",
             COUNT(IF(smses.tanggal = "'.$dateArray[0].'", smses.tanggal, NULL)) AS "'.$dateArray[0].'"
            ')
            ->leftJoin('phonebooks', 'smses.pengirim', '=', 'phonebooks.hp')
            ->whereRaw('smses.pesan LIKE "SRV#ADD%"')
            ->groupBy('smses.pengirim')
            ->get();

        return Excel::create($daerah, function($excel) use($agg) {

            $excel->sheet('aggregate', function($sheet) use($agg) {

                $sheet->appendRow(array(
                    'Jawaban', 'Jumlah(%)'
                ));

                foreach ($answers as $answer) {
                    $sheet->appendRow(array(
                        $answer->jawaban, round(($answer->jumlah/$sum)*100,2)
                    ));
                }

                $colors = array('#52ff7f','#ff7e0e','#9d8cf9');

                $sheet->rows(array(
                    array('Color', $colors[$nomor]),
                    array('Cumulative', 1),
                    array('Type', 'bar'),
                    array('LabelShow', 1),
                ));
            });

        })->export('csv');
    }

    public function surveySmsForDashboard($daerah)
    {
        $answers = array();

        $answers = DB::table('smses')
            ->selectRaw('answers.jawaban, count(answers.jawaban) as jumlah')
            ->leftJoin('surveys', 'surveys.id', '=', 'answers.survey_id')
            ->where(['answers.pertanyaan_id' => $nomor, 'surveys.kabupaten' => $daerah, 'surveys.tanggal' => '2015-10-22'])
            ->groupBy('answers.jawaban')
            ->orderBy('jumlah', 'desc')
            ->get();

        return Excel::create($daerah, function($excel) use($answers) {

            $excel->sheet('aggregate', function($sheet) use($answers) {

                $sheet->appendRow(array(
                    'Jawaban', 'Jumlah(%)'
                ));

                foreach ($answers as $answer) {
                    $sheet->appendRow(array(
                        $answer->jawaban, round(($answer->jumlah/$sum)*100,2)
                    ));
                }

                $colors = array('#52ff7f','#ff7e0e','#9d8cf9');

                $sheet->rows(array(
                    array('Color', $colors[$nomor]),
                    array('Cumulative', 1),
                    array('Type', 'bar'),
                    array('LabelShow', 1),
                ));
            });

        })->export('csv');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
