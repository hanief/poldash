<?php

namespace App\Http\Controllers;

use App\Option;
use App\Question;
use App\Rule;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class OptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($researchId, $ruleId, $questionId)
    {
        $question = Question::find($questionId);

        $options = Question::find($questionId)->options()->get();

        return view('option.index')->with(['options' => $options, 'researchId' => $researchId, 'ruleId' => $ruleId, 'question' => $question]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($researchId, $ruleId, $questionId)
    {
        $question = Question::find($questionId);

        return view('option.create')->with(['researchId' => $researchId, 'ruleId' => $ruleId, 'question' => $question]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($researchId, $ruleId, $questionId, Request $request)
    {
        $option = Question::find($questionId)->options()->create($request->all());;

        return redirect('research/'.$researchId.'/rules/'.$ruleId.'/questions/'.$questionId.'/options');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($researchId, $ruleId, $questionId, $id)
    {
        $question = Question::find($questionId);

        $option = Option::find($id);

        return view('option.edit')->with(['researchId' => $researchId, 'ruleId' => $ruleId, 'question' => $question, 'option' => $option]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($researchId, $ruleId, $questionId, Request $request, $id)
    {
        $option = Option::find($id)->update($request->all());

        return redirect('research/'.$researchId.'/rules/'.$ruleId.'/questions/'.$questionId.'/options');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($researchId, $ruleId, $questionId, $id)
    {
        $option = Option::find($id)->delete();

        return redirect('research/'.$researchId.'/rules/'.$ruleId.'/questions/'.$questionId.'/options');
    }
}
