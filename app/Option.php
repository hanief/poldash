<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'description',
        'number',
        'question_id'
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }
}
