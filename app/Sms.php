<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    protected $fillable = [
        'id',
        'tanggal',
        'waktu',
        'pesan',
        'pengirim'
    ];

    public function ddc() {
        return $this->belongsTo('App\Ddc');
    }

    protected $table = 'smses';
}
