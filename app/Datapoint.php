<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datapoint extends Model
{
    protected $fillable = [
        'id',
        'report_id',
        'research_id',
        'daerah',
        'tanggal'
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function report()
    {
        return $this->belongsTo('App\Report');
    }

    public function research()
    {
        return $this->belongsTo('App\Research');
    }
}
