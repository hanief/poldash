<?php

namespace App\Console;

use App\Report;
use App\Rule;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Sms;
use App\Ddc;
use App\Answer;
use App\Survey;
use App\Phonebook;
use Nathanmac\Utilities\Parser\Parser;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();

        $schedule->call(function() {
            //$this->readLiveSms();
            $this->grabLatestSmsWithoutParsing();
            //$this->grabLocalSms();
        })->everyMinute();

        $schedule->call(function() {
            $this->grabLatestPhoneBook();
        })->daily();
    }

    public function readLiveSms() {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://poltracking.zenziva.com/api/readsms.php?userkey=90gu40t&passkey=fa83ts');

        $parser = new Parser();
        $parsed = $parser->xml($response->getBody());

        $messages = $parsed['message'];

        Log::info($messages);

        if (!array_key_exists('text', $messages)) {
            $smses = array();

            foreach ($messages as $message) {
                $sms = Sms::find((int)$message['id']);
                if (is_null($sms)) {
                    $sms = $this->smsFromArray($message);
                    $smses[] = $sms;
                    $this->parseSmsContent($sms);
                }
            }
        }
    }

    public function grabLatestSms() {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://poltracking.zenziva.com/api/inboxgetall.php?userkey=90gu40t&passkey=fa83ts&status=all');

        $parser = new Parser();
        $parsed = $parser->xml($response->getBody());

        $messages = $parsed['message'];
        $smses = array();

        foreach($messages as $message) {
            $sms = Sms::find($message['id']);
            if (is_null($sms)) {
                $sms = $this->smsFromArray($message);
                $smses[] = $sms;
                $this->parseSmsContent($sms);
            }
        }
    }

    public function grabLatestSmsWithoutParsing() {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://poltracking.zenziva.com/api/inboxgetall.php?userkey=90gu40t&passkey=fa83ts&status=all');

        $parser = new Parser();
        $parsed = $parser->xml($response->getBody());

        $messages = $parsed['message'];
        $smses = array();

        foreach($messages as $message) {
            $sms = Sms::find($message['id']);
            if (is_null($sms)) {
                $sms = $this->smsFromArray($message);
                $smses[] = $sms;

                $report = Report::where('sms_id', $message['id'])->get()->first();
                if (is_null($report)) {
                    $report = Report::create([
                        'sms_id' => $message['id'],
                        'tanggal' => $sms->tanggal,
                        'waktu' => $sms->waktu,
                        'pesan' => $sms->pesan,
                        'pengirim' => $sms->pengirim,
                        'status' => false,
                    ]);
                }
            }
        }
    }

    public function grabLocalSms() {
        $smses = Sms::all();

        foreach($smses as $sms) {
            $this->parseSmsContent($sms);
        }
    }

    function smsFromArray($message) {
        if (array_key_exists('tgl', $message)) {
            $waktu = $message['waktu'];
            $tanggal = $message['tgl'];
        } else {
            $waktuCombined = explode(' ', $message['waktu']);
            $tanggal = $waktuCombined[0];
            $waktu = $waktuCombined[1];
        }
        $sms = Sms::create([
            'id' => (int)$message['id'],
            'tanggal' => $tanggal,
            'waktu' => $waktu,
            'pesan' => is_null($message['isiPesan']) ? '-' : $message['isiPesan'],
            'pengirim' => $message['dari']
        ]);

        return $sms;
    }


    function parseSmsContent(Sms $sms) {
        $smsArray = explode('#', $sms->pesan);

        if (strtolower($smsArray[0]) === 'ddc') {
            if (strtolower($smsArray[1]) === 'add') {
                $ddc = Ddc::where('sms_id',$sms->id)->first();
                if (is_null($ddc)) {
                    $ddc = Ddc::create([
                        'sms_id' => (int)$sms->id,
                        'nomor_kuesioner' => (int)$smsArray[2],
                        'daerah' => 'KUTIM',
                        'desa' => $smsArray[3]
                    ]);

                    for ($i = 4; $i < count($smsArray)-1; $i++){
                        $jawaban = $smsArray[$i];
                        $pertanyaanId = $i-3;

                        if ($pertanyaanId === 1) {
                            if ($jawaban === '(JAWABAN 1)1' || $jawaban === '1') {
                                $jawaban = 'POSITIF';
                            } else if ($jawaban === '(JAWABAN 1)2' || $jawaban === '2') {
                                $jawaban = 'NEGATIF';
                            } else if ($jawaban === '(JAWABAN 1)3' || $jawaban === '3') {
                                $jawaban = 'BIASA SAJA';
                            } else if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'POSITIF';
                            } else if (stripos($jawaban,'2') !== false) {
                                $jawaban = 'NEGATIF';
                            } else if (stripos($jawaban,'3') !== false) {
                                $jawaban = 'BIASA SAJA';
                            } else {
                                $pertanyaanId = 2;
                            }
                        }

                        if ($pertanyaanId === 2) {
                            if ($jawaban === '(JAWABAN 2+NAMA)1' || $jawaban === '1') {
                                $jawaban = 'BELUM';
                            } else if ($jawaban === '(JAWABAN 2+NAMA)2' || $jawaban === '2') {
                                $jawaban = 'SUDAH';
                            } else if ($jawaban === '(JAWABAN 2+NAMA)3' || $jawaban === '3') {
                                $jawaban = 'TIDAK TAHU/TIDAK JAWAB';
                            } else if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'BELUM';
                            } else if (stripos($jawaban,'norbaiti') !== false
                                || stripos($jawaban,'noorbaiti') !== false
                                || stripos($jawaban,'nurbaiti') !== false
                                || stripos($jawaban,'norbaeti') !== false
                                || stripos($jawaban,'nobaiti') !== false
                                || stripos($jawaban,'norbiati') !== false
                                || stripos($jawaban,'noor') !== false
                                || stripos($jawaban,'noah') !== false) {

                                $jawaban = 'Norbaiti - Ordiansyah';

                            } else if (stripos($jawaban,'ARDIANSYAH') !== false
                                || stripos($jawaban,'ardiansyah') !== false
                                || stripos($jawaban,'ardiansah') !== false
                                || stripos($jawaban,'ardyansyah') !== false
                                || stripos($jawaban,'ardyansah') !== false
                                || stripos($jawaban,'ardiyansah') !== false
                                || stripos($jawaban,'ardiyansyah') !== false
                                || stripos($jawaban,'ardiasyah') !== false
                                || stripos($jawaban,'alfian') !== false
                                || stripos($jawaban,'asaa') !== false) {

                                $jawaban = 'Ardiansyah - Alfian';

                            } else if (stripos($jawaban,'Ismunandar') !== false
                                || stripos($jawaban,'ismunandar') !== false
                                || stripos($jawaban,'Ismu') !== false
                                || stripos($jawaban,'ismu') !== false
                                || stripos($jawaban,'kasmidi') !== false) {
                                $jawaban = 'Ismunandar - Kasmidi';
                            } else if (stripos($jawaban,'rahasia') !== false) {
                                $jawaban = 'RAHASIA';
                            } else if (stripos($jawaban,'3') !== false) {
                                $jawaban = 'TIDAK TAHU/TIDAK JAWAB';
                            }
                        }

                        if ($pertanyaanId === 3) {
                            if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'TAHU';
                            } else if (stripos($jawaban,'2') !== false) {
                                $jawaban = 'TIDAK';
                            } else {
                                $pertanyaanId = 4;
                            }
                        }

                        if ($pertanyaanId === 4) {
                            if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'YA';
                            } else if (stripos($jawaban,'2') !== false) {
                                $jawaban = 'TIDAK';
                            } else if (stripos($jawaban,'3') !== false) {
                                $jawaban = 'TIDAK TAHU/TIDAK JAWAB';
                            } else if (stripos($jawaban,'rahasia') !== false) {
                                $jawaban = 'RAHASIA';
                            } else {
                                $pertanyaanId = 5;
                            }
                        }

                        if ($pertanyaanId === 5) {
                            if (stripos($jawaban,'listrik') !== false) {
                                $jawaban = 'LISTRIK';
                            } else if (stripos($jawaban,'jalan') !== false) {
                                $jawaban = 'JALAN';
                            } else if (stripos($jawaban,'pemekaran') !== false) {
                                $jawaban = 'PEMEKARAN';
                            } else if (stripos($jawaban,'pdam') !== false) {
                                $jawaban = 'PDAM';
                            } else if (stripos($jawaban,'kunjungan') !== false) {
                                $jawaban = 'KUNJUNGAN';
                            } else if (stripos($jawaban,'uang') !== false) {
                                $jawaban = 'UANG';
                            } else if (stripos($jawaban,'pupuksawit') !== false) {
                                $jawaban = 'PUPUK';
                            } else if (stripos($jawaban,'rumah') !== false) {
                                $jawaban = 'RUMAH';
                            } else if (stripos($jawaban,'air bersih') !== false || stripos($jawaban,'air') !== false) {
                                $jawaban = 'AIRBERSIH';
                            } else if (stripos($jawaban,'alat tani') !== false) {
                                $jawaban = 'ALATTANI';
                            } else if (stripos($jawaban,'peduli') !== false) {
                                $jawaban = 'PEDULI';
                            } else if (stripos($jawaban,'bibitsawit') !== false) {
                                $jawaban = 'BIBITSAWIT';
                            } else if (stripos($jawaban,'desa') !== false) {
                                $jawaban = 'DESAMAJU';
                            } else if (stripos($jawaban,'modalusaha') !== false || stripos($jawaban,'modal usaha') !== false) {
                                $jawaban = 'MODAL';
                            } else if (stripos($jawaban,'belum tahu') !== false || stripos($jawaban,'tidak ada') !== false || stripos($jawaban,'tidak tahu') !== false || stripos($jawaban,'tidaktahu') !== false || stripos($jawaban,'tidak jawab') !== false || stripos($jawaban,'tidakjawab') !== false) {
                                $jawaban = 'TT/TJ';
                            }
                        }

                        if ($pertanyaanId === 6) {
                            if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'YA';
                            } else if (stripos($jawaban,'2') !== false) {
                                $jawaban = 'TIDAK';
                            }
                        }
                        $deskripsi = '';

                        $answer = $ddc->answers()->create([
                            'kuesioner_id' => $ddc['nomor_kuesioner'],
                            'pertanyaan_id' => $pertanyaanId,
                            'jawaban' => $jawaban,
                            'deskripsi' => $deskripsi
                        ]);
                    }
                }

            }
        } else if (strtolower($smsArray[0]) === 'srv') {
            if (strtolower($smsArray[1]) === 'add') {
                $survey = Survey::where('sms_id',$sms->id)->first();
                if(is_null($survey)) {
                    $survey = Survey::create([
                        'sms_id' => (int)$sms->id,
                        'kabupaten' => 'kutim',
                        'daerah' => strtolower($smsArray[3]),
                        'tanggal' => $sms->tanggal
                    ]);

                    for ($i = 4; $i < count($smsArray); $i++) {
                        $jawaban = $smsArray[$i];
                        $pertanyaanId = $i - 3;

                        if ($pertanyaanId === 1) {
                            if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'Norbaiti - Ordiansyah';
                            } else if (stripos($jawaban,'2') !== false) {
                                $jawaban = 'Ardiansyah - Alfian';
                            } else if (stripos($jawaban,'3') !== false) {
                                $jawaban = 'Ismunandar - Kasmidi';
                            } else if (stripos($jawaban,'4') !== false) {
                                $jawaban = 'Tidak tahu/Tidak jawab';
                            } else {
                                $jawaban = 'Lainnya';
                            }
                        }

                        if ($pertanyaanId === 2) {
                            if (stripos($jawaban,'1') !== false) {
                                $jawaban = 'Norbaiti - Ordiansyah';
                            } else if (stripos($jawaban,'2') !== false) {
                                $jawaban = 'Ardiansyah - Alfian';
                            } else if (stripos($jawaban,'3') !== false) {
                                $jawaban = 'Ismunandar - Kasmidi';
                            } else if (stripos($jawaban,'4') !== false) {
                                $jawaban = 'Tidak tahu/Tidak jawab';
                            } else {
                                $jawaban = 'Lainnya';
                            }
                        }

                        $answer = $survey->answers()->create([
                            'kuesioner_id' => 0,
                            'pertanyaan_id' => $pertanyaanId,
                            'jawaban' => $jawaban,
                            'deskripsi' => ''
                        ]);
                    }
                }
            }
        } else if (stripos($smsArray[0], 'surnas') !== false) {
            //Log::info($smsArray);

            $survey = Survey::where('sms_id',$sms->id)->first();
            if(is_null($survey)) {
                $survey = Survey::create([
                    'sms_id' => (int)$sms->id,
                    'kabupaten' => 'nasional',
                    'daerah' => 'indonesia',
                    'tanggal' => $sms->tanggal
                ]);

                for ($i = 1; $i < count($smsArray); $i++) {
                    $jawaban = $smsArray[$i];
                    $pertanyaanId = $i;

                    if ($pertanyaanId === 1) {
                        if (stripos($jawaban, '1') !== false) {
                            $jawaban = 'Sangat tidak puas';
                        } else if (stripos($jawaban, '2') !== false) {
                            $jawaban = 'Kurang puas';
                        } else if (stripos($jawaban, '3') !== false) {
                            $jawaban = 'Cukup puas';
                        } else if (stripos($jawaban, '4') !== false) {
                            $jawaban = 'Sangat puas';
                        } else if (stripos($jawaban, '5') !== false) {
                            $jawaban = 'Tidak tahu/Tidak jawab';
                        } else {
                            $jawaban = 'Lainnya';
                        }
                    }

                    if ($pertanyaanId === 2) {
                        if (stripos($jawaban, '1') !== false) {
                            $jawaban = 'Sangat tidak puas';
                        } else if (stripos($jawaban, '2') !== false) {
                            $jawaban = 'Kurang puas';
                        } else if (stripos($jawaban, '3') !== false) {
                            $jawaban = 'Cukup puas';
                        } else if (stripos($jawaban, '4') !== false) {
                            $jawaban = 'Sangat puas';
                        } else if (stripos($jawaban, '5') !== false) {
                            $jawaban = 'Tidak tahu/Tidak jawab';
                        } else {
                            $jawaban = 'Lainnya';
                        }
                    }

                    $answer = $survey->answers()->create([
                        'kuesioner_id' => $survey->id,
                        'pertanyaan_id' => $pertanyaanId,
                        'jawaban' => $jawaban,
                        'deskripsi' => ''
                    ]);
                }
            }
        } else if (stripos($smsArray[0], 'karawang') !== false) {

            $survey = Survey::where('sms_id',$sms->id)->first();
            if(is_null($survey)) {
                $survey = Survey::create([
                    'sms_id' => (int)$sms->id,
                    'kabupaten' => 'karawang',
                    'daerah' => 'karawang',
                    'tanggal' => $sms->tanggal
                ]);

                for ($i = 1; $i < count($smsArray); $i++) {
                    $jawaban = $smsArray[$i];
                    $pertanyaanId = $i;



                    $answer = $survey->answers()->create([
                        'kuesioner_id' => $survey->id,
                        'pertanyaan_id' => $pertanyaanId,
                        'jawaban' => $jawaban,
                        'deskripsi' => ''
                    ]);
                }
            }
        } else if (stripos($smsArray[0], 'bateng') !== false) {
            $survey = Survey::where('sms_id',$sms->id)->first();
            if(is_null($survey)) {
                $survey = Survey::create([
                    'sms_id' => (int)$sms->id,
                    'kabupaten' => 'bateng',
                    'daerah' => 'bateng',
                    'tanggal' => $sms->tanggal
                ]);

                for ($i = 1; $i < count($smsArray); $i++) {
                    $jawaban = $smsArray[$i];
                    $pertanyaanId = $i;

                    $answer = $survey->answers()->create([
                        'kuesioner_id' => $survey->id,
                        'pertanyaan_id' => $pertanyaanId,
                        'jawaban' => $jawaban,
                        'deskripsi' => ''
                    ]);
                }
            }
        } else if (stripos($smsArray[0], 'kutim') !== false) {
            $survey = Survey::where('sms_id',$sms->id)->first();
            if(is_null($survey)) {
                $survey = Survey::create([
                    'sms_id' => (int)$sms->id,
                    'kabupaten' => 'kutim',
                    'daerah' => 'kutim',
                    'tanggal' => $sms->tanggal
                ]);

                for ($i = 1; $i < count($smsArray); $i++) {
                    $jawaban = $smsArray[$i];
                    $pertanyaanId = $i;

                    $answer = $survey->answers()->create([
                        'kuesioner_id' => $survey->id,
                        'pertanyaan_id' => $pertanyaanId-1,
                        'jawaban' => $jawaban,
                        'deskripsi' => ''
                    ]);
                }
            }
        }
    }

    function starts_with($haystack, $needle) {
        return substr($haystack, 0, strlen($needle)) === $needle;
    }

    function convertNumberFromLocalToInternational($number) {
        if (starts_with($number, '0')) {
            //$number = preg_replace('/0/','+62',$number,1);
            $pos = stripos($number,'0');
            if ($pos !== false) {
                $number = substr_replace($number,'+62',$pos,1);
            }
        }

        return $number;
    }

    function grabLatestPhoneBook() {
        $client = new \GuzzleHttp\Client();
        $response = $client->get('http://poltracking.zenziva.com/api/pbgetall.php?userkey=90gu40t&passkey=fa83ts');

        $parser = new Parser();
        $parsed = $parser->xml($response->getBody());

        $messages = $parsed['message'];
        $phonebooks = array();

        foreach($messages as $message) {
            $phonebook = Phonebook::find($message['id']);
            if (is_null($phonebook)) {
                $phonebook = $this->phonebookFromArray($message);
                $phonebooks[] = $phonebook;
            }
        }
    }

    function phonebookFromArray($message) {
        $phonebook = Phonebook::create([
            'id' => (int)$message['id'],
            'nama' => (!$message['nama']) ? '-' : $message['nama'],
            'alamat' => (!$message['alamat']) ? '-' : $message['alamat'],
            'kota' => (!$message['kota']) ? '-' : $message['kota'],
            'hp' => (!$message['hp']) ? '-' : $this->convertNumberFromLocalToInternational($message['hp']),
            'agama' => (!$message['agama']) ? '-' :  $message['agama'],
            'gender' => (!$message['jenisKelamin']) ? '-' :  $message['jenisKelamin'],
            'lahir' => (!$message['tglLahir']) ? '-' :  $message['tglLahir'],
            'pekerjaan' => (!$message['pekerjaan']) ? '-' :  $message['pekerjaan'],
            'grup' => (!$message['grup']) ? '-' :  $message['grup']
        ]);

        return $phonebook;
    }
}
