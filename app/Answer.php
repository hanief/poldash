<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'id',
        'datapoint_id',
        'kuesioner_id',
        'pertanyaan_id',
        'question_id',
        'option_id',
        'jawaban',
        'deskripsi'
    ];

    public function datapoint()
    {
        return $this->belongsTo('App\Datapoint');
    }

    public function question()
    {
        return $this->belongsTo('App\Question');
    }

    public function option()
    {
        return $this->belongsTo('App\Option');
    }
}
