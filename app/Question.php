<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'description',
        'type',
        'rule_id',
        'number'
    ];

    public function options()
    {
        return $this->hasMany('App\Option');
    }

    public function rule()
    {
        return $this->belongsTo('App\Rule');
    }
}
