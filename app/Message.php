<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //

    protected $fillable = [
        'type',
        'to',
        'msisdn',
        'messageId',
        'message-timestamp',
        'text',
        'keyword',
        'concat',
        'concat-ref',
        'concat-total',
        'concat-part',
        'data'
    ];
}
