<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    //

    protected $fillable = [
        'id',
        'sms_id',
        'tanggal',
        'waktu',
        'pesan',
        'pengirim',
        'status'
    ];

    public function sms() {
        return $this->belongsTo('App\Sms');
    }

    public function datapoints() {
        return $this->hasMany('App\Datapoint');
    }
}
