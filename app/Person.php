<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $fillable = [
        'nama',
        'alamat',
        'kota',
        'hp',
        'agama',
        'gender',
        'lahir',
        'pekerjaan',
        'grup'
    ];

    protected $table = 'persons';

}
