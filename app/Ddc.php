<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ddc extends Model
{
    protected $fillable = [
        'id',
        'sms_id',
        'daerah',
        'nomor_kuesioner',
        'desa'
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function sms()
    {
        return $this->belongsTo('App\Sms');
    }

    protected $table = 'ddcs';
}
