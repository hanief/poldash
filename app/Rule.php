<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule extends Model
{
    protected $fillable = [
        'keyword',
        'research_id'
    ];

    public function questions()
    {
        return $this->hasMany('App\Question');
    }

    public function research()
    {
        return $this->belongsTo('App\Research');
    }
}
