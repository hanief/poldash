<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'id',
        'sms_id',
        'kabupaten',
        'daerah',
        'tanggal'
    ];

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function sms()
    {
        return $this->belongsTo('App\Sms');
    }
}
