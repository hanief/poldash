angular.module('polSmsApp', ['ngAnimate', 'ngTouch', 'ui.grid','ui.grid.resizeColumns', 'ui.grid.moveColumns'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
})
    .controller('PolSMSController', ['$scope', '$http', 'uiGridConstants', function($scope, $http, uiGridConstants, $interval) {
        var polSmsCtrl = this;

        polSmsCtrl.page = 0;
        polSmsCtrl.smses = [];
        polSmsCtrl.aggregates = [];
        polSmsCtrl.volDateFilter = 'week';
        polSmsCtrl.dailySenderAggOption = 'all';
        volSmsDailyGridColDefs = [];

        polSmsCtrl.getSms = function() {
            $http.get('/api/sms/all').
                then(function(response) {
                    smsdata = response.data;
                    smsdata.forEach( function( row, index){
                        row.no = index+1;
                        row.count = row.pesan.split('#').length;
                    });
                    polSmsCtrl.allSmsGrid.data = smsdata;
                }, function(response) {
                    console.log(response);
                });
        };

        polSmsCtrl.getSmsDaily = function() {
            $http.get('/api/sms/all/daily').
                then(function(response) {
                    polSmsCtrl.allSmsDailyGrid.data = response.data;
                }, function(response) {
                    console.log(response);
                });
        };

        polSmsCtrl.getSenderAggregates = function() {
            $http.get('/api/ddc/aggregates/sender/'+polSmsCtrl.volDateFilter).
                then(function(response) {
                    polSmsCtrl.volSmsGrid.data = response.data;
                }, function(response) {
                    console.log(response);
                });
        };

        polSmsCtrl.getSenderAggregatesDaily = function() {
            $http.get('/api/research/'+polSmsCtrl.dailySenderAggOption+'/agg/sender/daily').
                then(function(response) {
                    smsdata = response.data;
                    //smsdata.forEach( function( row, index){
                    //    row.no = index+1;
                    //});
                    polSmsCtrl.volSmsDailyGrid.data = smsdata;

                    volSmsDailyGridColDefs = [
                        {field: 'nomor'},
                        {field: 'nama'},
                        {field: 'alamat'}
                    ];

                    for (i = 0; i < 7; i++) {
                        volSmsDailyGridColDefs.push({field: Object.keys(response.data[0])[2+i], type: 'number', aggregationType: uiGridConstants.aggregationTypes.sum});
                    }

                    volSmsDailyGridColDefs.push({field: 'total', type: 'number', aggregationType: uiGridConstants.aggregationTypes.sum});

                }, function(response) {
                    console.log(response);
                });
        };

        polSmsCtrl.getSms();
        polSmsCtrl.getSmsDaily();
        polSmsCtrl.getSenderAggregates();
        polSmsCtrl.getSenderAggregatesDaily();

        //$interval(polSmsCtrl.getSms, 60000);
        //$interval(polSmsCtrl.getSenderAggregates, 60000);
        //$interval(polSmsCtrl.getSenderAggregatesDaily, 60000);

        polSmsCtrl.allSmsGrid = {
            enableFiltering: true,
            enableColumnResizing: true,
            onRegisterApi: function(gridApi){
                polSmsCtrl.gridApi = gridApi;
            },
            columnDefs: [
                { field: 'no', type: 'number', width: '5%', enableColumnResizing: false },
                { field: 'id', type: 'number', width: '5%', enableColumnResizing: false },
                { field: 'tanggal', width: '10%'},
                { field: 'waktu', width: '10%' },
                { field: 'nama', width: '10%' },
                { field: 'hp', width: '10%' },
                { field: 'pesan', width: '40%' },
                { field: 'count', width: '10%' }
            ]
        };

        polSmsCtrl.allSmsDailyGrid = {
            onRegisterApi: function(gridApi){
                polSmsCtrl.gridApi = gridApi;
            }
        };

        polSmsCtrl.volSmsGrid = {
            enableFiltering: true,
            enableColumnResizing: true,
            onRegisterApi: function(gridApi){
                polSmsCtrl.gridApi = gridApi;
            },
            columnDefs: [
                { field: 'nomor', width: '30%', enableColumnResizing: false },
                { field: 'nama', width: '30%'},
                { field: 'jumlah', type: 'number', width: '40%' }
            ]
        };



        polSmsCtrl.volSmsDailyGrid = {
            showColumnFooter: false,
            enableFiltering: true,
            enableColumnResizing: true,
            onRegisterApi: function(gridApi){
                polSmsCtrl.gridApi = gridApi;
            },
            columnDefs: volSmsDailyGridColDefs
        };
    }]);