angular.module('polSmsApp', ['chart.js'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
}).controller('PolSMSController', ['$scope', '$http', function($scope, $http, $interval) {
    var polSmsCtrl = this;

    polSmsCtrl.data = [];
    polSmsCtrl.data0 = {};

    polSmsCtrl.defLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    polSmsCtrl.defData = [[65, 59, 80, 81, 56, 55, 40]];

    polSmsCtrl.labels = function($array) {
        var labelArray = new Array;
        for (i = 0; i < $array.length; i++) {
            labelArray.push($array[i].jawaban);
        }
        return labelArray;
    };

    polSmsCtrl.datas = function($array) {
        var dataArray = new Array;
        for (i = 0; i < $array.length; i++) {
            dataArray.push($array[i].jumlah);
        }
        return dataArray;
    };

    polSmsCtrl.processedData = function($array) {
        var dataArray = new Array;

        for (i = 0; i < $array.length; i++) {
            currentArray = $array[i];
            var object = {'labels':[], 'datas':[[]]};

            for (j = 0; j < currentArray.length; j++) {
                if (currentArray[j].jumlah > 20) {
                    object.labels.push(currentArray[j].jawaban);
                    object.datas[0].push(currentArray[j].jumlah);
                }
            }

            dataArray.push(object);
        }

        return dataArray;
    }

    polSmsCtrl.getAggregates = function() {
        $http.get('/api/ddc/aggregates').
            then(function(response) {
                //polSmsCtrl.data = response.data;
                polSmsCtrl.data = polSmsCtrl.processedData(response.data);
                polSmsCtrl.data0 = polSmsCtrl.data[0];
            }, function(response) {
                console.log(response);
            });
    };
    polSmsCtrl.getAggregates();
}]);