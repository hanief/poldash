angular.module('polSmsApp',['chart.js'], function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
})
    .controller('PolSMSController', ['$scope', '$http', function($scope, $http) {
        var polSmsCtrl = this;
        polSmsCtrl.page = 0;
        polSmsCtrl.smses = [];
        polSmsCtrl.aggregates = [];
        polSmsCtrl.aggregate1 = [];
        polSmsCtrl.jawaban = {};
        polSmsCtrl.jumlah = {};

        polSmsCtrl.getSms = function() {
            $http.get('http://sms.poltracking.com/api/sms/all').
                then(function(response) {
                    polSmsCtrl.smses = response.data;
                }, function(response) {
                    console.log(response);
                });
        };

        polSmsCtrl.getSms();

        polSmsCtrl.getAggregates = function() {
            $http.get('http://sms.poltracking.com/api/ddc/aggregates').
                then(function(response) {
                    polSmsCtrl.aggregates = response.data;
                }, function(response) {
                    console.log(response);
                });
        };

        polSmsCtrl.getAggregates();
    }]);