<!DOCTYPE html>
<html ng-app="polSmsApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PolDASH | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="PolSMSController as polSmsCtrl">

<canvas id="bar" class="chart chart-bar" chart-data="polSmsCtrl.data" chart-labels="polSmsCtrl.labels" chart-series="polSmsCtrl.series"></canvas>

<script src="/plugins/chartjs/Chart.min.js"></script>
<script src="/js/angular.js"></script>
<script src="/js/angular-chart.min.js"></script>
<script src="/js/ddc.dashboard.js"></script>
</body>
</html>