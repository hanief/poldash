<!DOCTYPE html>
<html ng-app="polSmsApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PolDASH | Dashboard | DDC</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/css/skins/skin-blue.min.css">
    <link rel="stylesheet" href="/css/angular-chart.css">
    <link rel="stylesheet" href="/css/ddc.dashboard.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="PolSMSController as polSmsCtrl">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>P</b>D</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Pol</b>DASH</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/img/avatar5.png" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/img/avatar5.png" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user()->name }}
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/avatar5.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <p><small>Administrator</small></p>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-envelope"></i> <span>SMS</span>
                        <!--<small class="label pull-right bg-yellow">12</small>-->
                    </a>
                </li>
                <li class="active">
                    <a href="/dashboard/ddc">
                        <i class="fa fa-home"></i> <span>Door to Door Campaign</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard/survey">
                        <i class="fa fa-question-circle"></i> <span>Survey</span>
                    </a>
                </li>
                <!--<li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Survey</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Candidate</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Volunteer</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Media</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Social Media</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>SMS Blast</span>
                    </a>
                </li>
                <li>
                    <a href="/dashboard">
                        <i class="fa fa-dashboard"></i> <span>Design</span>
                    </a>
                </li>-->
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Door to Door Campaign
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li class="active">DDC</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- Left col -->
                <div class="col-md-12">
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">1. Bagaimana respon tuan rumah yang dikunjungi? <small>(Jawaban: 1. Positif; 2. Negatif; 3. Biasa saja)</small></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="bar1" class="chart chart-bar" chart-data="polSmsCtrl.data[0].datas" chart-labels="polSmsCtrl.data[0].labels"></canvas>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">2. Apakah anda sudah punya pilihan calon Bupati Kutai Timur? <small>(Jawaban: 1. Belum; 2. Sudah, sebutkan... ; 3. Tidak tahu/Tidak jawab)</small></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="bar2" class="chart chart-bar" chart-data="polSmsCtrl.data[1].datas" chart-labels="polSmsCtrl.data[1].labels"></canvas>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">3. Apakah anda tahu Norbaiti Isran Noor?<small>(Jawaban: 1. Tahu; 2. Tidak;)</small></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="bar3" class="chart chart-bar" chart-data="polSmsCtrl.data[2].datas" chart-labels="polSmsCtrl.data[2].labels"></canvas>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">4. Apakah anda mau memilih Norbaiti Isran Noor?<small>(Jawaban: 1. Iya; 2. Tidak; 3. Tidak tahu/tidak jawab)</small></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="bar4" class="chart chart-bar" chart-data="polSmsCtrl.data[3].datas" chart-labels="polSmsCtrl.data[3].labels"></canvas>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">5. Apa yang harus dilakukan Norbaiti Isran Noor agar anda mau mencoblos beliau?</h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="bar5" class="chart chart-bar" chart-data="polSmsCtrl.data[4].datas" chart-labels="polSmsCtrl.data[4].labels"></canvas>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">6. Apakah responden mau kaca depan rumahnya ditempel stiker Norbaiti Isran Noor? <small>(Jawaban: 1. Ya; 2. Tidak;)</small></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body">
                            <canvas id="bar6" class="chart chart-bar" chart-data="polSmsCtrl.data[5].datas" chart-labels="polSmsCtrl.data[5].labels"></canvas>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div>
            </div>
        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.1.1
        </div>
        <strong>Copyright &copy; 2015 <a href="http://poltracking.com"> PT. Poltracking Indonesia</a>.</strong> All rights reserved. Theme provided by <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.
    </footer>

</div><!-- ./wrapper -->

<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/angular.js"></script>
<script src="/plugins/chartjs/Chart.js"></script>
<script src="/js/angular-chart.js"></script>
<script src="/js/ddc.dashboard.js"></script>
</body>
</html>
