@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Callback Ticketing</h3>
                </div>
                <div class="box-body">
                    <iframe src="https://docs.google.com/spreadsheets/d/1RsnYs1CZH7uRlAL4BOLkQCUCx4XdocKBMrL-pZ3SbeE/edit?usp=sharing" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop