<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Poltracking Indonesia</title>

    <!-- CSS -->
    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/simplex/bootstrap.min.css" rel="stylesheet" integrity="sha256-4nVETqQoIoCwuephcXpJ501G8B5sgBHb1ZsKU/D476I= sha512-cfSmkkLRDAcUNaJxRRWopCyEGX43UkWCAOl2wErYMBGOQVWwOsZ7IFuXScF9H/6nMGbmsgV4m5/xYfesyvHTxw==" crossorigin="anonymous">
</head>
<body ng-controller="PolSMSController as polSMSCtrl">
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1>Maintenance Mode</h1>
                <p>Mohon maaf situs Poltracking Indonesia sedang dalam perawatan.</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
