<!DOCTYPE html>
<html ng-app="polSmsApp">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>PolDASH | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="/css/ui-grid.min.css">
    <link rel="stylesheet" href="/css/styles.css">
    <link rel="stylesheet" href="/css/style.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition skin-blue sidebar-mini" ng-controller="PolSMSController as polSmsCtrl">
<div class="wrapper">

    <header class="main-header">

        <!-- Logo -->
        <a href="index2.html" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>P</b>D</span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Pol</b>DASH</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <!-- Navbar Right Menu -->
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="/img/avatar5.png" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="/img/avatar5.png" class="img-circle" alt="User Image">
                                <p>
                                    {{ Auth::user()->name }}
                                </p>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="/auth/logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>

        </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="/img/avatar5.png" class="img-circle" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <p><small>@if (Auth::user()->admin) Administrator @else Operator @endif</small></p>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li @if(Request::is('dashboard')) class="active" @endif>
                    <a href="/dashboard">
                        <i class="fa fa-envelope"></i> <span>SMS</span>
                        <!--<small class="label pull-right bg-yellow">12</small>-->
                    </a>
                </li>
                @if (Auth::user()->approved)
                    @if (Auth::user()->admin)
                        <li @if(Request::is('callback')) class="active" @endif>
                            <a href="/callback">
                                <i class="fa fa-phone-square"></i> <span>Callback</span>
                            </a>
                        </li>
                        <li @if(Request::is('research')) class="active" @endif>
                            <a href="/research">
                                <i class="fa fa-question-circle"></i> <span>Riset</span>
                            </a>
                        </li>
                    @endif
                        <li class="@if(Request::is('quick-count/*')) class="active" @endif treeview">
                            <a href="#">
                                <i class="fa fa-dashboard"></i> <span>Quick Count</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li @if(Request::is('quick-count/bateng')) class="active" @endif><a href="/quick-count/bateng"><i class="fa fa-circle-o"></i> Bangka Tengah</a></li>
                                <li @if(Request::is('quick-count/babar')) class="active" @endif><a href="/quick-count/babar"><i class="fa fa-circle-o"></i> Bangka Barat</a></li>
                                <li @if(Request::is('quick-count/basel')) class="active" @endif><a href="/quick-count/basel"><i class="fa fa-circle-o"></i> Bangka Selatan</a></li>
                                <li @if(Request::is('quick-count/beltim')) class="active" @endif><a href="/quick-count/beltim"><i class="fa fa-circle-o"></i> Belitung Timur</a></li>
                                <li @if(Request::is('quick-count/kutim')) class="active" @endif><a href="/quick-count/kutim"><i class="fa fa-circle-o"></i> Kutai Timur</a></li>
                            </ul>
                        </li>
                @endif
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div><!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 0.1.1
        </div>
        <strong>Copyright &copy; 2015 <a href="http://poltracking.com"> PT. Poltracking Indonesia</a>.</strong> All rights reserved. Theme provided by <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.
    </footer>

</div><!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="/js/jquery.min.js"></script>
<!-- Bootstrap 3.3.5 -->
<script src="/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="/js/app.min.js"></script>
<!-- Sparkline -->
<script src="/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- SlimScroll -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="/plugins/chartjs/Chart.min.js"></script>
<!-- AngularJS -->
<script src="/js/angular.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-touch.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.3/angular-animate.js"></script>
<script src="/js/ui-grid.min.js"></script>
<script src="/js/angular-chart.min.js"></script>
<script src="{{ url('/js/deleteHandler.js') }}"></script>
<script src="/js/polsms.js"></script>
<script src="/js/site.js"></script>

@yield('scripts')

</body>
</html>
