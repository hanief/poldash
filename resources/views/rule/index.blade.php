@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Aturan Riset
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href={{ url('research') }}>Riset</a></li>
        <li class="active">Aturan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Aturan Riset <small>{{ $research->title }}</small></h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Nomor</th>
                                <th>Keyword</th>
                                <th>Aksi</th>
                            </tr>
                            @foreach($rules as $key => $rule)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $rule->keyword }}</td>
                                    <td>
                                        <a class="btn btn-success" href={{ url('research/'.$research->id.'/rules/'.$rule->id.'/questions') }}>Pertanyaan</a>
                                        <a class="btn btn-info" href={{ url('research/'.$research->id.'/rules/'.$rule->id.'/edit') }}>Edit</a>
                                        <a href={{ url('research/'.$research->id.'/rules/'.$rule->id) }} data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda yakin akan menghapus aturan?" class="btn btn-danger">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Tambah Aturan Riset <small>{{ $research->title }}</small></h3>
                </div><!-- /.box-header -->
                    {!! BootForm::open()->action( route('rules.store', ['id' => $research->id], $rule = new App\Rule) )->post() !!}
                    {!! BootForm::bind($rule) !!}
                    <div class="box-body">
                        @include('rule.form', ['submitButtonText' => 'Tambah Aturan'])
                    </div><!-- /.box-body -->
                    {!! BootForm::close() !!}
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop