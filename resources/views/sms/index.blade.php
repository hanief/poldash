<!DOCTYPE html>
<html lang="en" ng-app="polSmsApp">
<head>
    <meta charset="UTF-8">
    <title>Poltracking Indonesia</title>

    <!-- CSS -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/css/angular-chart.css">

    <!-- JS -->
    <script src="/js/jquery.min.js" type="text/javascript"></script>
    <script src="/js/angular.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/Chart.js"></script>
    <script src="/js/angular-chart.js"></script>
    <script src="/js/polsms.js"></script>
</head>
<body ng-controller="PolSMSController as polSmsCtrl">
<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">PolSMS</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li><a ng-click="polSmsCtrl.getSms()"><span class="glyphicon glyphicon-refresh"></span> Refresh data</a></li>
                <li><a href="#">Login</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h1>DDC Kutim</h1>
            </div>
            <h2>Daftar SMS</h2>
            <h3>Jumlah SMS diterima : <span class="text-success">[[ polSmsCtrl.smses.length ]]</span> buah</h3>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>No</th>
                        <th>Tanggal</th>
                        <th>Waktu</th>
                        <th>Pengirim</th>
                        <th>Pesan</th>
                    </tr>
                    <tr ng-repeat="sms in polSmsCtrl.smses | orderBy:[sms.tanggal, sms.waktu]:true | limitTo:15:polSmsCtrl.page*15">
                        <td>[[ $index+1 ]]</td>
                        <td>[[ sms.tanggal ]]</td>
                        <td>[[ sms.waktu ]]</td>
                        <td>[[ sms.pengirim ]]</td>
                        <td>[[ sms.pesan ]]</td>
                    </tr>
                </table>
            </div>
            <h2>Pertanyaan 1</h2>
            <div class="table-responsive">
                <table class="table table-striped">
                    <tr>
                        <th>Jawaban</th>
                        <th>Jumlah</th>
                    </tr>
                    <div ng-repeat="aggr in polSmsCtrl.aggregates">
                        <tr ng-repeat="(key, val) in aggr">
                            <td>[[ key ]]</td>
                            <td>[[ val ]]</td>
                        </tr>
                    </div>

                    <canvas id="bar" class="chart chart-bar"
                            chart-data="polSmsCtrl.aggregates[0][jumlah]"
                            chart-labels="polSmsCtrl.aggregates[0][jawaban]">
                    </canvas>

                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>