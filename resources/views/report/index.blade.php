@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Report</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Daftar Report</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="reports-table">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Tanggal</th>
                                <th>Pesan</th>
                                <th>Count</th>
                                <th>Pengirim</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href={{ route('reports.create') }} class="btn btn-primary">Tambah Report</a>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

@push('scripts')
<script>
    $(function() {
        var table = $('#reports-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "desc" ]],
            ajax: {
                url: '{!! route('reports.reports') !!}'
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'tanggal', name: 'tanggal' },
                { data: 'pesan', name: 'pesan' },
                { data: 'count', name: 'count' },
                { data: 'pengirim', name: 'pengirim' },
                { data: 'status', name: 'status' },
                { data: 'aksi', name: 'aksi' }
            ]
        });
    });
</script>
@endpush

@stop