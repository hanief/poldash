@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Edit Report
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/reports"><i class="fa fa-dashboard"></i> Report</a></li>
        <li class="active">Edit</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Detail Report</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    {!! BootForm::open()->action( route('reports.update', $report) )->put() !!}
                    {!! BootForm::bind($report) !!}
                    {!! BootForm::text('Tanggal', 'tanggal') !!}
                    {!! BootForm::text('Waktu', 'waktu') !!}
                    {!! BootForm::text('Pesan', 'pesan') !!}
                    {!! BootForm::text('Pengirim', 'pengirim') !!}
                    {!! BootForm::checkbox('Approved', 'status') !!}
                    {!! BootForm::submit('Simpan') !!}
                    {!! BootForm::close() !!}
                </div>

            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop