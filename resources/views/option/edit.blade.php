@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Ubah Pilihan Jawaban
    </h1>
    <ol class="breadcrumb">
        <li><a href={{ route('research.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Riset</a></li>
        <li><a href={{ route('rules.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Aturan</a></li>
        <li><a href={{ route('questions.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Pertanyaan</a></li>
        <li><a href={{ route('options.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Pilihan Jawaban</a></li>
        <li class="active">Ubah Pilihan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">{{ $question->description }}</h3>
                </div><!-- /.box-header -->

                {!! BootForm::open()->action( route('options.update', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id, 'id' => $option->id], $option) )->put() !!}
                {!! BootForm::bind($option) !!}
                <div class="box-body">
                    @include('option.form', ['submitButtonText' => 'Simpan Pilihan'])
                </div><!-- /.box-body -->
                {!! BootForm::close() !!}
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop