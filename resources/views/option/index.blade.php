@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pilihan Jawaban
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href={{ route('research.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Riset</a></li>
        <li><a href={{ route('rules.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Aturan</a></li>
        <li><a href={{ route('questions.index', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }}>Pertanyaan</a></li>
        <li class="active">Pilihan jawaban</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">{{ $question->description }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Nomor</th>
                                <th>Deskripsi jawaban</th>
                                <th>Tipe</th>
                            </tr>
                            @foreach($options as $key => $option)
                                <tr>
                                    <td>{{ $option->number }}</td>
                                    <td>{{ $option->description }}</td>
                                    <td>
                                        <a class="btn btn-info" href={{ route('options.edit', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id, 'id' => $option->id]) }}>Edit</a>
                                        <a href={{ route('options.destroy', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id, 'id' => $option->id]) }} data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda yakin akan menghapus pilihan?" class="btn btn-danger">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href={{ route('options.create', ['researchId' => $researchId, 'ruleId' => $ruleId, 'questionId' => $question->id]) }} class="btn btn-default">Tambah pilihan</a>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop