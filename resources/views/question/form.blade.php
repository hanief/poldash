{!! BootForm::text('Nomor pertanyaan', 'number')->placeholder('Isi dengan nomor pertanyaan') !!}
{!! BootForm::textarea('Deskripsi', 'description')->rows(5) !!}

{!! BootForm::select('Tipe Jawaban', 'type')->options(['multiple' => 'Pilihan ganda', 'text' => 'Teks', 'identifier' => 'Pengenal', 'combo' => 'Kombinasi pilihan ganda dan teks'])->select('multiple'); !!}
{!! BootForm::submit($submitButtonText) !!}