@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pertanyaan Riset
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href={{ route('research.index', ['researchId' => $researchId]) }}>Riset</a></li>
        <li><a href={{ route('rules.index', ['researchId' => $researchId]) }}>Aturan</a></li>
        <li class="active">Pertanyaan</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Daftar Pertanyaan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table">
                            <tr>
                                <th>Nomor</th>
                                <th>Deskripsi</th>
                                <th>Tipe</th>
                            </tr>
                            @foreach($questions as $key => $question)
                                <tr>
                                    <td>{{ $question->number }}</td>
                                    <td>{{ $question->description }}</td>
                                    <td>{{ $question->type }}</td>
                                    <td>
                                        @if ($question->type === "multiple" || $question->type === "combo") <a class="btn btn-success" href={{ route('options.index', ['researchId' => $researchId, 'ruleId' => $rule->id, 'questionId' => $question->id]) }}>Pilihan Jawaban</a>@endif
                                        <a class="btn btn-info" href={{ route('questions.edit', ['researchId' => $researchId, 'ruleId' => $rule->id, 'questionId' => $question->id]) }}>Edit</a>
                                        <a href={{ route('questions.destroy', ['researchId' => $researchId, 'ruleId' => $rule->id, 'questionId' => $question->id]) }} data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda yakin akan menghapus kuis?" class="btn btn-danger">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    <a href={{ route('questions.create', ['researchId' => $researchId, 'ruleId' => $rule->id]) }} class="btn btn-default">Tambah pertanyaan</a>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop