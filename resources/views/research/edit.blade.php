@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tambah Riset
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/research"><i class="fa fa-dashboard"></i> Riset</a></li>
        <li class="active">Tambah</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Daftar Riset</h3>
                </div><!-- /.box-header -->

                {!! BootForm::open()->action( route('research.update', $research) )->put() !!}
                {!! BootForm::bind($research) !!}
                <div class="box-body">
                    @include('research.form', ['submitButtonText' => 'Simpan Riset'])
                </div><!-- /.box-body -->
                {!! BootForm::close() !!}
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop