@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Riset
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Riset</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <div class="box box-info">
                    <div class="box-header">
                        <h3 class="box-title">Daftar Riset</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>Nomor</th>
                                            <th>Judul</th>
                                            <th>Tipe</th>
                                            <th>Daerah</th>
                                            <th>Mulai</th>
                                            <th>Selesai</th>
                                            <th>Aksi</th>
                                        </tr>
                                        @foreach($researches as $key => $research)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $research->title }}</td>
                                                <td>{{ $research->type }}</td>
                                                <td>{{ $research->area }}</td>
                                                <td>{{ $research->start_date }}</td>
                                                <td>{{ $research->end_date }}</td>
                                                <td>
                                                    <a class="btn btn-success" href={{ url('research/'.$research->id.'/rules') }}>Aturan</a>
                                                    <a class="btn btn-info" href="/research/{{ $research->id }}/edit">Edit</a>
                                                    <a href={{ url('research/'.$research->id) }} data-method="delete" data-token="{{csrf_token()}}" data-confirm="Anda yakin akan menghapus kuis?" class="btn btn-danger">Hapus</a>
                                                    @if ($research->area == "KUTIM") <a class="btn btn-warning" href="https://app.cyfe.com/dashboards/259425/tv" target="_blank">Display</a>@endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <a href="/research/create" class="btn btn-success">Tambah Riset</a>
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@stop