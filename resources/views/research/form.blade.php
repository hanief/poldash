{!! BootForm::text('Judul', 'title')->placeholder('Judul riset') !!}
{!! BootForm::select('Tipe', 'type')->options(['survey' => 'Survei', 'ddc' => 'DDC', 'quickcount' => 'Quick Count', 'realcount' => 'Real Count'])->select('survey'); !!}
{!! BootForm::text('Daerah', 'area')->placeholder('Daerah tempat riset') !!}
{!! BootForm::text('Tanggal Mulai', 'start_date')->placeholder('YYYY-MM-DD. Contoh: 2015-12-31.') !!}
{!! BootForm::text('Tanggal Berakhir', 'end_date')->placeholder('YYYY-MM-DD. Contoh: 2015-12-31.') !!}
{!! BootForm::submit($submitButtonText) !!}