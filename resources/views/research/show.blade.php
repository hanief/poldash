@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Dashboard
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">SMS Masuk</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" ng-click="polSmsCtrl.getSms()"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div ui-grid="polSmsCtrl.allSmsGrid" class="todaySmsTable"></div>
                    <br>
                    <div ui-grid="polSmsCtrl.allSmsDailyGrid" class="todaySmsDailyTable"></div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">SMS Harian Per Relawan</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" ng-click="polSmsCtrl.getSenderAggregatesDaily()"><i class="fa fa-refresh"></i></button>
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div ui-grid="polSmsCtrl.volSmsDailyGrid" class="volunteerSmsDailyTable"></div>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix">
                    <div class="row">
                        <div class="col-xs-3">
                            <select class="form-control pull-right" required="true" ng-model="polSmsCtrl.dailySenderAggOption">
                                <option value="all" selected>Semua</option>
                                <option value="kutim">DDC - Kutim</option>
                                <option value="bateng">DDC - Bateng</option>
                                <option value="bateng">Survey - Bateng</option>
                                <option value="kutim">Survey - Kutim</option>
                                <option value="karawang">Survey - Karawang</option>
                                <option value="nasional">Survey - Nasional</option>
                                <option value="basel">Survey - Basel</option>
                                <option value="babar">Survey - Babar</option>
                            </select>
                        </div>
                        <div class="col-xs-3">
                            <button class="btn btn-default" ng-click="polSmsCtrl.getSenderAggregatesDaily()"><i class="fa fa-refresh"></i></button>
                        </div>
                    </div>
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Callback Ticketing</h3>
                </div>
                <div class="box-body">
                    <iframe src="https://docs.google.com/spreadsheets/d/1RsnYs1CZH7uRlAL4BOLkQCUCx4XdocKBMrL-pZ3SbeE/edit?usp=sharing" width="100%" height="500px"></iframe>
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop