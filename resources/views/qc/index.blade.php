@extends('app')

@section('content')
        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Quick Count
    </h1>
    <ol class="breadcrumb">
        <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Quick Count</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Main row -->
    <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Bangka Tengah</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="bateng-table">
                            <thead>
                            <tr>
                                <th>Desa/Kel</th>
                                <th>Nama</th>
                                <th>HP</th>
                                <th>SMS</th>
                                <th>Jawaban</th>
                                <th>Masuk</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Bangka Barat</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="babar-table">
                            <thead>
                            <tr>
                                <th>Desa/Kel</th>
                                <th>Nama</th>
                                <th>HP</th>
                                <th>SMS</th>
                                <th>Jawaban</th>
                                <th>Masuk</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Bangka Selatan</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="basel-table">
                            <thead>
                            <tr>
                                <th>Desa/Kel</th>
                                <th>Nama</th>
                                <th>HP</th>
                                <th>SMS</th>
                                <th>Jawaban</th>
                                <th>Masuk</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

            <div class="box box-info">
                <div class="box-header">
                    <h3 class="box-title">Belitung Timur</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table" id="beltim-table">
                            <thead>
                            <tr>
                                <th>Desa/Kel</th>
                                <th>Nama</th>
                                <th>HP</th>
                                <th>SMS</th>
                                <th>Jawaban</th>
                                <th>Masuk</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>

                        </table>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->

@push('scripts')
<script>
    $(function() {
        $('#bateng-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "asc" ]],
            ajax: {
                url: '/quick-count/qcs/bateng'
            },
            columns: [
                { data: 'alamat', name: 'alamat' },
                { data: 'nama', name: 'nama' },
                { data: 'hp', name: 'hp'},
                { data: 'pesan', name: 'pesan' },
                { data: 'count', name: 'count' },
                { data: 'waktu', name: 'waktu' },
                { data: 'status', name: 'status' },
                { data: 'aksi', name: 'aksi' }
            ]
        });

        $('#babar-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "asc" ]],
            ajax: {
                url: '/quick-count/qcs/babar'
            },
            columns: [
                { data: 'alamat', name: 'alamat' },
                { data: 'nama', name: 'nama' },
                { data: 'hp', name: 'hp'},
                { data: 'pesan', name: 'pesan' },
                { data: 'count', name: 'count' },
                { data: 'waktu', name: 'waktu' },
                { data: 'status', name: 'status' },
                { data: 'aksi', name: 'aksi' }
            ]
        });

        $('#basel-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "asc" ]],
            ajax: {
                url: '/quick-count/qcs/basel'
            },
            columns: [
                { data: 'alamat', name: 'alamat' },
                { data: 'nama', name: 'nama' },
                { data: 'hp', name: 'hp'},
                { data: 'pesan', name: 'pesan' },
                { data: 'count', name: 'count' },
                { data: 'waktu', name: 'waktu' },
                { data: 'status', name: 'status' },
                { data: 'aksi', name: 'aksi' }
            ]
        });

        $('#beltim-table').DataTable({
            processing: true,
            serverSide: true,
            order: [[ 0, "asc" ]],
            ajax: {
                url: '/quick-count/qcs/beltim'
            },
            columns: [
                { data: 'alamat', name: 'alamat' },
                { data: 'nama', name: 'nama' },
                { data: 'hp', name: 'hp'},
                { data: 'pesan', name: 'pesan' },
                { data: 'count', name: 'count' },
                { data: 'waktu', name: 'waktu' },
                { data: 'status', name: 'status' },
                { data: 'aksi', name: 'aksi' }
            ]
        });
    });
</script>
@endpush

@stop